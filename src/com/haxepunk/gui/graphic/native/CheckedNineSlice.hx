package com.haxepunk.gui.graphic.native;

import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.graphics.atlas.AtlasRegion;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;

class CheckedNineSlice extends NineSlice
{

	/**
	 * Create an Image composed by a NineSlice and a "checked" Image. Used for checkbox or similar behaviour composants.
	 * @param	width		Initial Width of the 9slice
	 * @param	height		Initial Height of the 9slice
	 * @param	nineSliceClipRect	Rectangle of the first Slice area on the skin
	 * @param	checkClipRect		Rectangle of the total area on the skin to use for check graphic
	 * @param	skin		optional custom skin
	 */
	public function new(width:Float, height:Float, ?nineSliceClipRect:Rectangle, ?checkClipRect:Rectangle, ?skin:AtlasRegion)
	{
		super(width, height, nineSliceClipRect, skin);

		_checkGraphic = _skin.clip(checkClipRect);
		_checkClipRect = checkClipRect;

		_scale = new Point();
		_checkOffset = new Point();
	}

	/**
	 * Render the image on top of the parent.
	 * @param	target
	 * @param	point
	 * @param	camera
	 */
	override public function renderAtlas(layer:Int, point:Point, camera:Point)
	{
		super.renderAtlas(layer, point, camera);
		
		_scale.x = this.width / _checkClipRect.width;
		if (_scale.x > 2) _scale.x = Math.round(_scale.x*2)/2;
		else if (_scale.x > 1) _scale.x = Math.round(_scale.x);
		_scale.y = this.height / _checkClipRect.height;
		if (_scale.y > 2) _scale.y = Math.round(_scale.y*2)/2;
		else if (_scale.y > 1) _scale.y = Math.round(_scale.y);
		
		_checkOffset.x = this.width / 2 - (_checkClipRect.width * _scale.x) / 2;
		_checkOffset.y = this.height / 2 - (_checkClipRect.height * _scale.y) / 2;
		
		_checkGraphic.draw(_point.x + _checkOffset.x * HXP.screen.fullScaleX, _point.y + _checkOffset.y * HXP.screen.fullScaleY, layer, _scale.x * HXP.screen.fullScaleX, _scale.y * HXP.screen.fullScaleY);
	}

	private var _checkGraphic:AtlasRegion;
	private var _checkOffset:Point;
	private var _scale:Point;
	var _checkClipRect:Rectangle;

}